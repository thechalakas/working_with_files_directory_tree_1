﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Working_With_Files_Directory_Tree_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //lets call this method which will traverse the directory tree
            //but first we need the following 4 things

            //a directory object to get directory details
            //this is also the directory where stuff begin, you know current level 0
            DirectoryInfo directory_object = new DirectoryInfo(@"E:\ayyo");

            //a string that represents what subset of directories we are searching for.
            //here I am setting the search term as to find any diretory that has the letter e in the directory name
            string search_phrase_directories = "*";

            //a integer that represents how deep we wish to dig
            //I am setting it to 3. this means, suppose the starting location of our search has a directory, which has 10 levels of directories
            //inside it. despite that, our search will after going till 3 directory depth
            int maximum_depth_for_traversing_directories = 3;

            //a integer that represents the current level, which is usually zero
            //this is 0, because this is where we are starting. it is sort of like how we write 
            //for i=0 in for loop. it is our starting point, and we usually start at 0
            int current_level = 0;

            //lets call the method that does the searching for us. 

            search_the_directory_method(directory_object, search_phrase_directories, maximum_depth_for_traversing_directories, current_level);

            Console.WriteLine("That's all folks!!!");

            //this is here to prevent the console window from dissapearing
            Console.ReadLine();
        }

        //this is the method that will do a directory search and display what it found
        //the neccessary stuff such as 
        //directory object with the current directory set
        //max level of depth to search
        //current level
        //and the search phrase to use while doing the directory traversal
        //has to be provided
        private static void search_the_directory_method(DirectoryInfo directory_object, string search_phrase_directories, int maximum_depth_for_traversing_directories, int current_level)
        {
            //first make sure that current level is lower than max depth
            //after all, we dont want to search depth level 5, if current level is already 6
            if(current_level >= maximum_depth_for_traversing_directories)
            {
                return;
            }

            //since we are doing directory search, we are pretty much assuming that we will have full access
            //what if we dont.
            //this is why we have this catch block. Of course, it is always a good idea to have these try catch statements
            //but you know it is that much more important when working with directories and files becuase you dont when a access is there
            //and when a access is removed.

            try
            {
                //get a list of all directories in the current path
                DirectoryInfo[] list_of_all_directories = directory_object.GetDirectories(search_phrase_directories);

                //now, loop through each directory, and then recursively call each directory 
                //thanks to the return statement right at the being of this method, the recursiveness will stop once max depth is reached

                foreach(DirectoryInfo current_directory in list_of_all_directories)
                {
                    //display the directory you are currently in.

                    Console.WriteLine("current drive is {0} and level is {1}  - {0}",
                        current_directory.Name,current_level);

                    //get the list of all files in the current directory
                    var list_of_all_files = current_directory.GetFiles();

                    foreach(FileInfo temp_file in list_of_all_files)
                    {
                        //display file name
                        Console.WriteLine("current drive is {0} and file name  is {1}  - {0}",
                        current_directory.Name, temp_file.Name);
                    }

                    Console.WriteLine("current drive is {0} and total number of files is  {1}  - {0}",
                        current_directory.Name, list_of_all_files.Length);

                    Console.WriteLine("--------------------------------FILES DISPLAYED----------------------");

                    Console.ReadLine();
                    //now do a recursive call and get to work and find out the directories in this directory
                    //here we are sending
                    //the current directory object, because that what we wish to scan and report
                    //maximum depth will remain same as originally sent
                    //search phrase will remain same
                    //current level will increase by 1, becuase we are going one level down. So, if we were in level 0, then as we dig in
                    //we are moving on to level 1. if the current_level+1, is not done, then current level will be stuck at 0, and this 
                    //recursive call will never end.
                    search_the_directory_method(current_directory, search_phrase_directories, maximum_depth_for_traversing_directories, current_level + 1);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("drive user permission error - {0}", e.ToString());
            }
        }

        //this method will traverse the directory

    }
}












